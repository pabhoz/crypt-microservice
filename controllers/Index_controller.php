<?php

class Index_controller extends Controller {

	function __construct() {
		parent::__construct();
	}
        
        public function getIndex($data,$algo = "SHA512"){
            if(!isset($data)){
                throw new Exception("No data to encrypt");
            }
            $response = [
               "status"=>200,
                "encrypted_data"=>Hash::create($algo, $data, HASH_PASSWORD_KEY)
            ];
            Penelope::printJSON($response);

        }
        
}
