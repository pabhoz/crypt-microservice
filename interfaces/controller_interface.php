<?php
/**
 *
 * @author pabhoz
 */
interface controller_interface {
    
    public function __construct();
    public function get($method,$params = null);
    public function post($method,$params = null);
    public function put($method,$params = null);
    public function delete($method,$params = null);
    
}
